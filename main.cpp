#include <iostream>

int main()
{
	static size_t count = 100;
	std::cout << "Hello Git!";
	if (count-- == 0)
	{
		// here it is a comment
		std::cout<<"Code4Fun";
		return 0;
	}
	return main();
}
